import { Component, OnInit } from '@angular/core'
import { UseCase } from '../usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd'
    },
    {
      id: 'UC-02',
      name: 'Speler toevoegen',
      description: 'Hiermee kan een administrator een speler toevoegen.',
      scenario: [
        'De admin klikt op de voeg speler toe knop.',
        'De admin vult een formulier in met de gegevens van de speler en klikt op de toevoeg knop.',
        'De applicatie voegt de speler toe indien alle velden ingevuld zijn.'
      ],
      actor: 'Administrator',
      precondition: 'De actor is ingelogd',
      postcondition: 'De speler is toegevoegd.'
    },
    {
      id: 'UC-03',
      name: 'Team toevoegen',
      description: 'Hiermee kan een gebruiker een team aanmaken.',
      scenario: [
        'De gebruiker logt in en kiest een land van supporten.',
        'De applicatie genereerd een begin team voor de gebruiker.',
        'De applicatie redirect naar het gemaakte team.'
      ],
      actor: 'Gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'Het team is toegevoegd.'
    },
    {
      id: 'UC-04',
      name: 'Toernooi toevoegen',
      description: 'Hiermee kan een administrator een toernooi toevoegen.',
      scenario: [
        'De admin klikt op de voeg toernooi toe knop.',
        'De admin vult een formulier in met de gegevens van het toernooi en klikt op de toevoeg knop.',
        'De applicatie voegt het toernooi toe indien alle velden ingevuld zijn.'
      ],
      actor: 'Administrator',
      precondition: 'De actor is ingelogd',
      postcondition: 'Een toernooi is toegevoegd.'
    },
    {
      id: 'UC-05',
      name: 'Wedstrijd spelen',
      description: 'Hiermee kan een gebruiker een wedstrijd spelen met zijn of haar gemaakte team.',
      scenario: [
        'De gebruiker klikt op de tegenstanders knop.',
        'De gebruiker kiest een tegenstander uit en klikt op de knop begin.',
        'De applicatie simuleert de wedstrijd laat de uitslag zien aan de gebruiker na een enkele tijd.'
      ],
      actor: 'Gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'De gebruiker krijgt geld voor de gespeelde wedstrijd.'
    }
  ]

  title = 'Galaxy voetbal'
  naam = 'Jazz Walter'
  studentnummer = '2138718'

  beschrijving =
    'Een galaxy voetbal applicatie, waar gebruikers zelf spelers kunnen kiezen en toevoegen aan een zelf gemaakt team, spelers kosten geld en geld is te winnen door wedstrijden te spelen tegen een ander gekozen team die door een andere gebruiker is gemaakt. Een admin kan spelers & toernooien aanmaken, veranderen en verwijderen. Ik wil mongoDB gebruiken als noSQL database omdat ik met deze database simpelweg de meeste ervaring heb en er al een beetje mee overweg kan, en deze database is big data database wat handig is aangezien er vele teams en spelers in de database komen.'

  constructor() {}

  ngOnInit() {}
}
